from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired



class TowSearchForm(FlaskForm):

    keyword = StringField('', validators=[DataRequired(message='Please Enter Company')], render_kw={"placeholder": "Search For Company"})
    search = SubmitField('Search')
