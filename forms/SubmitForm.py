from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, RadioField, FloatField
from wtforms.validators import DataRequired, Length

rating_values = [('Good', 'Good'), ('Average', 'Average'), ('Bad', 'Bad')]

class SubmitForm(FlaskForm):
    company = StringField('Company', validators=[DataRequired(message='Company Required')])
    price = FloatField('Price:', validators=[DataRequired(message='Price Required and Must be Numbers Only')])
    distance = FloatField('Distance (in miles):', validators=[DataRequired(message='Distance Required')])
    estimate = FloatField('Estimated Price:')
    rating = RadioField('Rating Your Service:', choices=rating_values, validators=[DataRequired(message='Please leave a Rating')])
    comment = TextAreaField('Comment:',validators=[Length(min=0, max=250, message=('Comment cannot be longer than 250 chars.'))])
    submit = SubmitField('Submit Tow Info')

