alembic==1.0.7
Babel==2.6.0
Click==7.0
dominate==2.3.5
Flask==1.0.2
Flask-Babel==0.12.2
Flask-Bootstrap4==4.0.2
Flask-Migrate==2.3.1
Flask-SQLAlchemy==2.3.2
Flask-Table==0.5.0
Flask-WTF==0.14.2
gunicorn==19.9.0
itsdangerous==1.1.0
Jinja2==2.10
Mako==1.0.7
MarkupSafe==1.1.0
python-dateutil==2.7.5
python-editor==1.0.3
pytz==2018.9
six==1.12.0
SQLAlchemy==1.2.17
visitor==0.1.3
Werkzeug==0.14.1
WTForms==2.2.1
