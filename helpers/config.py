import os

basedir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

def get_config_object():
    if os.environ.get('DEV'):
        return Development
    return Production


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'adsjklhafjklaljajklnf fadfdf dfgsdg'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class Development(Config):
    DEVELOPMENT = True
    #SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://{user}:{passwd}@{host}:{port}/{db}'.format(user='postgres',passwd='0978Chipper0978',host='127.0.0.1',port=5432,db='towing-flask')



class Production(Config):
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://{user}:{passwd}@{host}:{port}/{db}'.format(user=os.environ.get('PG_USER'),passwd=os.environ.get('PG_PASSWORD'),host=os.environ.get('DATABASE_URL'),port=5432,db=os.environ.get('DB_NAME'))


