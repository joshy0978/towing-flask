from app import db
from database.models import TowEntry
from sqlalchemy.sql import func

def search_query(search_keyword):
    print(search_keyword)
    return TowEntry.query.filter(TowEntry.company.contains(search_keyword)).all()

def id_query(id):
    return TowEntry.query.filter_by(id=id).first_or_404()

def query_last_entries(limit=10):
    return TowEntry.query.order_by(TowEntry.id.desc()).limit(limit)

def get_towing_company_info(tow_company):

    price_avg = db.session.query(func.avg(TowEntry.price)).filter(TowEntry.company == tow_company).scalar()
    estimate_avg = db.session.query(func.avg(TowEntry.estimate)).filter(TowEntry.company == tow_company).scalar()
    good_count = db.session.query(TowEntry).filter(TowEntry.company.like(tow_company)).filter(TowEntry.rating.like('Good')).count()
    average_count = db.session.query(TowEntry).filter(TowEntry.company.like(tow_company)).filter(TowEntry.rating.like('Average')).count()
    bad_count = db.session.query(TowEntry).filter(TowEntry.company.like(tow_company)).filter(TowEntry.rating.like('Bad')).count()
    tow_history = db.session.query(TowEntry).filter(TowEntry.company == tow_company).all()
    num_tows = len(tow_history)
    return {'company_name': tow_company, "good_count":good_count, "average_count":average_count,
            "bad_count": bad_count, "tow_history":tow_history,
            "num_tows": num_tows, "price_avg": price_avg, "estimate_avg": estimate_avg}

def add_towing_entry(tow_data, ip):
    print(tow_data)
    company = tow_data['company']
    price = round(float(tow_data['price']),2)
    distance = round(float(tow_data['distance']),2)
    estimate = None
    if tow_data['estimate']:
        estimate = round(float(tow_data['estimate']),2)
    comment = tow_data['comment']
    rating = tow_data['rating']
    tow_entry = TowEntry(company=company, price=price, distance=distance, comment=comment, estimate=estimate, rating=rating, ip=ip)
    db.session.add(tow_entry)
    db.session.commit()