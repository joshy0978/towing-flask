from app import db
from sqlalchemy.orm import relationship


class Logger(db.Model):
    __tablename__ = 'logger'

    id = db.Column(db.Integer, primary_key=True)
    ip = db.Column(db.Text, index=True, unique=False)
    first_visit = db.Column(db.DateTime, default=db.func.current_timestamp())
    last_visit = db.Column(db.DateTime, default=db.func.current_timestamp())
    user_agent = db.Column(db.Text, index=True)


    def __tablename(self):
        return "logger"

class TowEntry(db.Model):
    __tablename__ = 'tow_entry'

    id = db.Column(db.Integer, primary_key=True)
    company = db.Column(db.Text, index=True, unique=False)
    price = db.Column(db.REAL, index=True, unique=False)
    distance = db.Column(db.REAL, index=True, unique=False)
    comment = db.Column(db.Text, index=True, unique=False)
    estimate = db.Column(db.REAL, index=True, unique=False)
    rating = db.Column(db.Text, index=True, unique=False)
    ip = db.Column(db.Text, index=True, unique=False)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())

    def __repr__(self):
        return f'<Company {self.company}>'

    def __str__(self):
        return f'<Company {self.company}>'

    def __tablename(self):
        return "tow_entry"













