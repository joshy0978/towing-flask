from flask import Flask
from helpers.config import get_config_object
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_bootstrap import Bootstrap

app = Flask(__name__)
app.config.from_object(get_config_object())

db = SQLAlchemy(app)
migrate = Migrate(app, db)

boot = Bootstrap(app)

from app import routes