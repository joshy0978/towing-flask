from flask import render_template, redirect, flash, request, url_for
from app import app
from forms.SubmitForm import SubmitForm
from forms.SearchForm import TowSearchForm
from database.DbHelpers import add_towing_entry, id_query, search_query, query_last_entries, get_towing_company_info

@app.route('/', methods=['GET', 'POST'])
@app.route('/home', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def home():
    # print(request.remote_addr)

     # create Tow Search Entry for Nav Bar
    form = TowSearchForm()

    # -----Handle Get Requests-------
    if request.method == 'GET':
        entries = query_last_entries(limit=10)
        return render_template('home.html', title='Crook County Towing', entries=entries, form=form)

    # -----Handle Post Requests-------
    if len(form.keyword.data) > 0:
       #  redirect to search results
        return search_results(form.keyword.data)
    else:
        flash("Please enter valid data")
        return redirect(url_for('home'))



@app.route('/info/<int:id>', methods=['GET', 'POST'])
def info(id):

    #create Tow Search Entry for Nav Bar
    form = TowSearchForm()

    # -----Handle Get Requests-------
    if request.method == 'GET':
        entry = id_query(id)
        company_info = get_towing_company_info(entry.company)

        return render_template('info.html', entry=entry, form=form, company_info=company_info)

    # -----Handle Post Requests-------
    if form.validate_on_submit():
       # if valid search results
       # pass value to search_results function
        return search_results(form.keyword.data)
    else:
        flash("Please enter valid data")
        return redirect(url_for('home'))



@app.route('/results', methods=['GET'])
def search_results(search_string):

    #create Tow Search Entry for Nav Bar
    form = TowSearchForm()

    #Get Entries From Database
    entries = search_query(search_string)

    # If no entries
    # redirect to home and flash message
    # not results found
    if not entries:
        flash('No results found!')
        return redirect(url_for('home'))


    return render_template('search_results.html', entries=entries, form=form)

@app.route('/about', methods=['GET', 'POST'])
def about():

    # create Tow Search Entry for Nav Bar
    form = TowSearchForm()

    # -----Handle Get Requests-------
    if request.method == 'GET':
        return render_template('about.html', title='About', form=form)

    # -----Handle Post Requests------
    if form.validate_on_submit():
        #  redirect to search results
        return search_results(form.keyword.data)
    else:
        flash("Please enter valid data")
        return redirect(url_for("home"))



@app.route('/submit', methods=['GET', 'POST'])
def submit():

    # create Tow Search Entry for Nav Bar
    form = TowSearchForm()

    # Form to submit Tow entry
    search_form = SubmitForm()

    # -----Handle Get Requests-------
    if request.method == 'GET':
        return render_template('submit.html', title='', form=form, search_form=search_form)

    # -----Handle Post Requests------

    # Search Form
    if form.validate_on_submit() or search_form.validate_on_submit():
        if len(form.keyword.data) > 0:
            return search_results(form.keyword.data)
        else:
            add_towing_entry(search_form.data, request.remote_addr)
            flash('Thank you for your submission')
            return redirect(url_for('home'))
    else:
        if form.keyword.data:
            flash("Please enter a company to search.")
            return redirect(url_for('home.html'))
        else:
            flash("Please Enter Data Correctly")
            return redirect(url_for('submit'))


