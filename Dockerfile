FROM python:3.7-alpine

RUN adduser -D towing

WORKDIR /home/towing-flask

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev
RUN pip install psycopg2-binary

COPY app app
COPY migrations migrations
COPY towing.py ./ boot.sh ./
RUN chmod +x boot.sh


RUN chown -R towing:towing ./
USER towing

EXPOSE 5090

ENTRYPOINT ["./boot.sh"]